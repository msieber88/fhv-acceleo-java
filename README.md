# Getting started

1. Clone the repository
2. Import into Eclipse
3. Add new Run configuraton
![alt text](run_configuration.png)

# Project overview
## at.fhv.example
Acceleo source code for generating java applications including the main file which is used to start the generator

## at.fhv.example.profile
Custom UML Profile used to generate JavaBeans

## at.fhv.example.project
Empty project in which the generated code will be placed

## at.fhv.example.uml
UML diagram used as basis for the code generation